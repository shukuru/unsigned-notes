# Unsigned Notes

Personal documentation and static notes, nothing more ;)

### License

This project is released under [MIT License](https://spdx.org/licenses/MIT.html).

All documents are generated with [Sphinx](https://www.sphinx-doc.org/en/master/index.html) using [Furo](https://github.com/pradyunsg/furo) theme.
