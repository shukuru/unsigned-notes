..
   Copyright (c) 2022 Shukuru
   Licensed under MIT License
   SPDX-License-Identifier: MIT

******************************
Welcome to my unsigned notes !
******************************

About Me
========

.. figure:: _static/avatar.jpg
    :align: left
    :alt: Shukuru's avatar

Hello guys, my name is **Shukuru**.
Self-taught programmer, I spend my spare time to experiment and learn various things such
as languages, algorithms or technologies.

My favorite problem domain is **Electronic Engineering** and I put efforts to
make my path through this exciting subject in order to **build expertise in OS/Kernel theory**.

I wrote theses pages to share my discoveries and my works. But I'm convinced that the best way
to understand something is to write about it and try to explain it as well as possible to
someone else, they will therefore be written for this purpose.

.. caution:: 
    Please be aware that all documents will be constantly corrected 
    as I'm learning and can lack of precision in some points. But feel
    free to contact me if you potentially can correct them 😉

.. image:: _static/gitlab-svgrepo-com.svg
    :align: left
    :width: 32
    :height: 32
    :target: https://gitlab.com/shukuru
    :alt: GitLab repository

.. image:: _static/twitter-svgrepo-com.svg
    :align: right
    :width: 32
    :height: 32
    :target: https://twitter.com/ShukuruWorld
    :alt: Twitter profile

.. toctree::
    :maxdepth: 2
    :glob:
    :titlesonly:
    :hidden:

    ee.rst
    languages.rst
    os_and_kernel_theory.rst
    emulation.rst
