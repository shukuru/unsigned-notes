..
   Copyright (c) 2022 Shukuru
   Licensed under MIT License
   SPDX-License-Identifier: MIT

******************
OS & Kernel theory
******************

This section regroups all OS & Kernel theory related notes.

.. toctree::
    :maxdepth: 2
    :glob:
    :titlesonly:

    os_and_kernel_theory/*.rst