..
   Copyright (c) 2022 Shukuru
   Licensed under MIT License
   SPDX-License-Identifier: MIT

*********************
Programming languages
*********************

This section regroups all programming languages related notes.

.. toctree::
    :maxdepth: 2
    :glob:
    :titlesonly:

    languages/*.rst