..
   Copyright (c) 2022 Shukuru
   Licensed under MIT License
   SPDX-License-Identifier: MIT

.. warning:: 
    Learning in progress, it can lack of precision

************************
Electricity basics stuff
************************

This note tends to explain basics concepts of electricity, a prerequisite to learn
OS & Kernel theory.

Learning C programming or assembly language for a specific architecture isn't enough
to understand the problem domain of this subject. It's like wanting to implements a
financial managing software without knowing a bunch of economy.

----

Static electricity
==================

Experiment
^^^^^^^^^^

Have you ever experimented rubbing a inflatable ballon on wool clothes then putting it
in a short distance of hair ? Mostly os us have and could see hairs were attracted by
the ballon.

Another example is the surprise of an electric choc after touching a metal material
such as a car door.

This is static electricity, the first experiment of electric flow by humans in the
history.

The atom composition
^^^^^^^^^^^^^^^^^^^^
Each atom is basically compound of three elements called `particles`:
    - Protons
    - Neutrons
    - Electrons

Protons and Neutrons are very tightly bound together in a cluster called `nucleus`
at the center of the atom. Electrons, however, orbits more or less far around the core.

An atom is often represented with the Rutherford-Bohr's model as following:

.. figure:: /_static/ee/copper_atom_rutherford-bohr-dark.drawio.svg
    :align: center
    :alt: Atom schema
    :scale: 70
    :class: only-dark

.. figure:: /_static/ee/copper_atom_rutherford-bohr-light.drawio.svg
    :align: center
    :alt: Atom schema
    :scale: 70
    :class: only-light

    Crude copper atom is compound of 29 protons, 35 neutrons and 29 electrons

Due to the `strong nuclear interaction force <https://en.wikipedia.org/wiki/Strong_interaction/>`_
which exercises a force in a very short range, nucleus elements can't be moved easily from the atom core.
Understand it by adding or removing one of them.

Protons determines the atomic number Z, combined with neutrons they determines the atomic mass A.

Electrons, due to their relative small weight and distance from the nucleus, have far more 
freedom to move whether in the atom or in empty space between nearby ones.

In our subject, we will bear an interest to protons and electrons. Neutrons role is more 
related to atom radioactivity and nuclear properties.

Atom charge
^^^^^^^^^^^

It has been observed the following rules:
    - Protons repulses protons
    - Electrons repulses electrons
    - Protons attracts electrons

.. note:: 
    The unique reason that protons stays sticked together is the strong nuclear
    interaction force. Remember, it works in very short distances at atomic scale.

We can also note that protons carries a positive charge and electrons a negative one.

Basically, atoms carries the same count of protons and electrons.

But when an electron leaves or arrives, it puts the atom in an imbalanced state called charged state,causing interactions with charged particles and atoms nearby.

In terms, the electrons permits the electric conductivity. They also permits thermal
conductivity but we will talking about this in the `Resistance <#Resistance>`_ sub-part.

The another concept that plays a major role in the electrical conductivity,
is the principle of valence.

Valence electrons
^^^^^^^^^^^^^^^^^

If we take again our example of Copper atom represented in Rutherford-Bohr's model, we can see that
the representation disposes electrons on a several circular orbits around the nucleus.

Theses orbits also called shells, carries a different count of electrons than each others and the most outer one is called valence.

The valence is, in simple words, the electrons count on the most outer shell of the atom and can be between 1 and 8.

The more electrons are present at this location, the harder will be the electrical conductivity; remember, electrons repulses other electrons.

In an copper atom, the valence is 1. This make a copper material a good conductor.

That's it, the lower the valence, the better the conductivity.

.. note:: 
    Valence electrons are also called free electrons

We can also note that valence electrons are the far most particles from nucleus and therefore 
those that needs the less energy to flow on nearby atoms valence.

Conductivity
============

The conductivity is determined by the freedom of valence electrons to move between
nearby atoms. Some materials permits a high conductivity such as copper while some others
doesn't permit the electrical conductivity such as glass.

We therefore talking about conductors and insulators, divided in 3 groups:
    - Conductors or metals: Copper, Gold, Silver (1-3 covalent bonds)
    - Semiconductors or non-metals: Carbon (4 covalent bonds)
    - Insulators: Nitrogen, Neon (5-8 covalent bonds)
  
As we can see, all conductors doesn't share the same conductivity power as for insulators.

Free electrons move in the empty space between atoms in a flow, pushing their friend ahead of it.
So if we want to make an electron flow between two points, we will need to use something to define
a path: wires, in a good conductive material obviously.

.. note:: 
    No matter the thickness of the wire can be, the electrons will flow on the entire
    surface of it.

As long the wire will remain continuous, the electron flow will be.

.. figure:: /_static/ee/continuous_flow-dark.drawio.svg
    :align: center
    :alt: Continuous, broke and bonded electron flows
    :class: only-dark

.. figure:: /_static/ee/continuous_flow-light.drawio.svg
    :align: center
    :alt: Continuous, broke and bonded electron flows
    :class: only-light

    Continuous, broke and bonded electron flows

However, as air is - fortunately - a good insulator, if you break the wire at some point, you will
break the electron flow too. To let the electron flow again, you must bond this gap with
conductive material.

In a electrical circuit, you design the electron path between different components to
manipulate the flow.

Circuits
========

A circuit is no more than an never-ending electron flow loop, making it's path through
the wires and various components. All you need to do is to maintain a continuous flow with
a power source at a specific rate, which refers to voltage and current in the next section.

In a circuit, the electrical continuity is primordial, any break in the pathway no matter
where it occurs in, will prevent the charge flow throughout the entire circuit.

Voltage and Current
===================

.. admonition:: TODO
    :class: note
    
    This content is under redacting

Resistance
==========

.. admonition:: TODO
    :class: note
    
    This content is under redacting

Ohm law
=======

.. admonition:: TODO
    :class: note

    This content is under redacting

----

**Learning sources**
    - https://www.allaboutcircuits.com/textbook/
    - https://www.electrical4u.com/valence-electron/
    - https://en.wikipedia.org/wiki/Electron/
    - https://en.wikipedia.org/wiki/Strong_interaction/
    - https://en.wikipedia.org/wiki/Bohr_model/
